#' Set MAUI snapshot - Compare for Two snapshots
#' 
#' @param con  an RODBC connection
#' @param SnapshotType  The snapshot type 
#' @return logical value
#' @details
#'    Set MAUI snapshot
#'
#' @import RODBC
#' @import getPass
#' @import methods
#' @export
setSnapshotCompare <- function(con, SnapshotType,SnapshotId)
{
  err <- 0
  out = NA
  SnapshotId_1 <-paste0("'", min(SnapshotId),"'")
  SnapshotId_2 <- paste0("'", max(SnapshotId),"'")
  SnapshotType <- paste0("'", max(SnapshotType),"'")
  tryCatch(out<- sqlQuery(channel = con, 
                          paste("(SELECT MAUI.set_snapshot_compare(", SnapshotType,",",SnapshotId_1,",",SnapshotType,",",SnapshotId_2,") FROM dual)", sep = ""),
                          errors=TRUE),
           warning = function(w){
             print("Warning in setSnapshot:")
             print(w)
             err <<- err + 1
           },
           error = function(e){
             print("Error in setSnapshot:")
             print(e)
             err <<- err + 1
           }
  )
  if (err > 0)
  {
    stop("Unable to set snapshot.")
  }
  return(TRUE)
}
