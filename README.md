MAUIDW: Tools for interacting with the MAUI data warehouse
=======================================================================

Permissions
------------------
In order to use institutional data from the University of Iowa
Data Warehouse, you will need:

1. To complete a human subjects research training (CITI)
2. To complete FERPA training
3. To apply for access to the data warehouse using the [MAUI Online Access Request Form](https://apps.its.uiowa.edu/forms/maui-access)


Operating System
---------------------
This package is only tested on Microsoft Windows operating systems, due to a dependency 
on the RODBC library and the Oracle11g driver. This is not a fundamental 
limitation, and we hope to
generalize the library in the future to enable productive use of high performance
computing infrastructure available on campus. 


Data Source
---------------
This package interfaces with the data warehouse via ODBC. For those who 
are not familiar with ODBC, an overview is available [here](https://technet.microsoft.com/en-us/library/ms188681(v=sql.105).aspx).
In short, ODBC allows a user to configure the operating system to recognize
a standard data source, such as a SQL server or Oracle database. This 
allows applications, such as R, to interface with data sources without  
writing source specific code. 

For this project, we need to interface with an Oracle database. This will
require the installation of the Oracle 11g client. 

Some brief instructions on the installation of the client are available [here](https://its.uiowa.edu/support/article/2057), though you will
need to work with IT to make sure it is configured correctly. 

Once your machine is configured, you will need to set up an ODBC data source called 'dwprod', which points to the MAUI database. This data source may be either a "user DSN" or "System DSN", as categorized by the ODBC Data Source Administrator. 

R Configuration
-----------------
<h5> Environment </h5>
This project is organized as an R package, and requires R version 3.3 or greater. 
R is available for download [here](https://cran.r-project.org/), and does not
require administrative permissions to install or update. We also recommend the
Rstudio IDE, available [here](https://www.rstudio.com/), which will require 
administrative rights to install (IT can help).

MAUIDW depends on several other R packages, so once R and RStudio are installed, 
open RStudio and run the following code in the console window:

    install.packages(c("devtools", "dplyr", "tidyr", "RODBC", "getPass"), dependencies = TRUE)


Next, clone a copy of the library from [github.uiowa.edu](https://github.uiowa.edu/CPHS/MAUIDW), or download a zip file and extract it. The package can then be installed by running:


    library(devtools)
    install(pkg="MAUIDW-master",  local=TRUE)
    
The "pkg" argument should be the path to the MAUIDW package folder. In the 
code above, the "MAUIDW-master" folder was located in the current working directory, which can be changed in the "Session" menu of RStudio. 

Development Environment: setup
----------------------------------

If you want to contribute to the development of MAUIDW, some additional
steps are recommended. 

After installing R, make sure it is available on your [system
path](https://en.wikipedia.org/wiki/PATH_(variable)). This can be accomplished by searching for "Edit the environment variables for your account" in the start menu. Either 
create, or edit the existing "PATH" variable to include the "bin\x64" directory associated with your R installation

This directory will likely be something like:

> C:\\Users\\(hawk ID)\\Documents\\R\\R-3.3.0

where "(hawk ID)" will be replaced by your hawk-ID. 

Directories in the "PATH" variable are separated by semicolons. 

Additional recommended R packages are:

1. knitr
2. rmarkdown
3. testthat


Development Environment: workflow
----------------------------------

The authoritative source for working with/on R packages is [Writing R Extensions](https://cran.r-project.org/doc/manuals/r-release/R-exts.html), 
however it is a very large and dense document, unsuitable for beginners. 

<h5>Package Structure</h5>
(more information coming)
<h5>Package Documentation</h5>

We use the 'roxygen2' package for documentation. Help 
pages for functions are generated from specially formatted
comments which precede them; for examples, see the source code. 

To compile the documentation, simply use the 'document' function
from the 'devtools' library:

    library(devtools)
    document("/path/to/MAUIDW")


<h5>Installing the Package</h5>

After making modifications to the package, the new version can be installed
in the same way described above. Alternatively, packages can be installed
from the command line using the command:

    R CMD INSTALL <package folder name>
    
Additionally, any unit tests can be run with:

    R CMD check <package folder name>
    
After installing the package, you will need to restart your R session 
for the changes to take effect. 







