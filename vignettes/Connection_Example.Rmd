---
title: "MAUIDW Examples"
author: "Grant Brown"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Vignette Title}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

Introduction
---------------

This vignette has several basic examples of querying data from
the data warehouse. 

Connect and Query Data
-----------------------------

```{r cars}
suppressPackageStartupMessages({
library(MAUIDW)
library(dplyr)
library(tidyr)
library(knitr)
})
con <- createMAUIDWConn()

# Execute a query using "makeTableQuery"
STUD_ALUM_RELATIONSHIP <- makeTableQuery(con, 
               tableName = "STUD_ALUM_RELATIONSHIP",
               fieldList = list("MASTER_ID", 
                                "STUD_RELATIONSHIP_KEY"), 
               subsetQuery = "select  DISTINCT aas.MASTER_ID from 
                                      (MAUI.APPL_PROGRAM_OF_STUDY pos 
                                      INNER JOIN MAUI.APPL_APPLICATION aas on 
                                      pos.MASTER_ID = aas.MASTER_ID AND 
                                      pos.APPL_APPLICATION_ID = aas.APPL_APPLICATION_ID)
                         WHERE (COMBINED_APPLICATION_TYPE = 'PRIMARY' OR 
                                COMBINED_APPLICATION_TYPE is null) 
                         AND IS_APPLICATION_PRIMARY_POS = 'Y'
                         AND ADMN_HANDLING_CD_KEY = 'A' 
                         AND (aas.ADMN_DECISION_TYPE_EN = 'ADMITTED' OR 
                             aas.ADMN_DECISION_TYPE_EN = 'NO_DECISION' OR 
                             aas.ADMN_DECISION_TYPE_EN = 'DEFERRED'    OR 
                             aas.ADMN_DECISION_TYPE_EN = 'WAITLISTED')
                         AND aas.SESSION_CD = 20153")
# Execute a raw SQL query:
birthStates <- sqlQuery(con, "select BIRTH_STATE_DESCR
                        from MAUI.MSTR_PERSON mps INNER JOIN (SELECT 
                                      DISTINCT aas.MASTER_ID from 
                                      (MAUI.APPL_PROGRAM_OF_STUDY pos 
                                      INNER JOIN MAUI.APPL_APPLICATION aas on 
                                      pos.MASTER_ID = aas.MASTER_ID AND 
                                      pos.APPL_APPLICATION_ID = aas.APPL_APPLICATION_ID)
                         WHERE (COMBINED_APPLICATION_TYPE = 'PRIMARY' OR 
                                COMBINED_APPLICATION_TYPE is null) 
                         AND IS_APPLICATION_PRIMARY_POS = 'Y'
                         AND ADMN_HANDLING_CD_KEY = 'A' 
                         AND (aas.ADMN_DECISION_TYPE_EN = 'ADMITTED' OR 
                             aas.ADMN_DECISION_TYPE_EN = 'NO_DECISION' OR 
                             aas.ADMN_DECISION_TYPE_EN = 'DEFERRED'    OR 
                             aas.ADMN_DECISION_TYPE_EN = 'WAITLISTED')
                         AND aas.SESSION_CD = 20153) sbst ON mps.MASTER_ID = sbst.MASTER_ID")

# Check if a snapshot exists:
snapshotExists(con, SnapshotType = "CENSUS", SnapshotId = "20143")

# Set the snapshot for the connection
setSnapshot(con, SnapshotType = "CENSUS", SnapshotId = "20143")

# Run a query on the snapshotted connection
STUD_ALUM_RELATIONSHIP <- makeTableQuery(con, 
               tableName = "STUD_ALUM_RELATIONSHIP",
               fieldList = list("MASTER_ID", 
                                "STUD_RELATIONSHIP_KEY"))

# Close the connection
close(con)
```

Simple Data Summaries
------------------------
```{r}
table(birthStates$BIRTH_STATE_DESCR)
table(STUD_ALUM_RELATIONSHIP$STUD_RELATIONSHIP_KEY)
```

Nicely formatted Summaries
------------------------------
```{r}
reldata <- as.data.frame(table(STUD_ALUM_RELATIONSHIP$STUD_RELATIONSHIP_KEY))
colnames(reldata) <- c("Relation", "Frequency")
kable(reldata)
```

